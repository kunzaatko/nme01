#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import re

def dNt_dt(t,Nt):
    return (1 + math.cos(t))*Nt

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])
interval = (0,5)

def interval_len(interval):
    return interval[1] - interval[0]

def euler_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':'Eulers method','marker':'o','markersize':'2','ls':'--'}):# {{{
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step*difference(t,func[-1]))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)# }}}

def mid_point_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':'mid point method','marker':'o','markersize':'2','ls':'--'}):# {{{
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step*(difference(t+step/2,func[-1]))/func[-1]*(func[-1]+step/2*difference(t,func[-1])))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)# }}}

def heuns_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':"Heun's method",'marker':'o','markersize':'2','ls':'--'}):# {{{
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step/2*(difference(t,func[-1]) + difference(t + step, func[-1] + step*difference(t,func[-1]))))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)# }}}

def runge_kutts_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict={'label':"Runge-Kutta's method of the fourth degree",'marker':'o','markersize':'2','ls':'--'}):# {{{
    """
    *init_cond* must be on the right side of the interval
    """
    def evaluate_k(difference,xn,yn,step):
        k1 = difference(xn,yn)
        k2 = difference(xn + step/2, yn + k1/2*step)
        k3 = difference(xn + step/2,yn + k2/2*step)
        k4 = difference(xn + step, yn + k3*step)
        return (k1, k2, k3, k4)
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        k1, k2, k3, k4 = evaluate_k(difference,t,func[-1],step)
        func.append(func[-1] + step/6*(k1 + 2*k2 + 2*k3 + k4))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)# }}}}}}


number_of_points = 100
# runge_kutts_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax)

def func(t):
    return math.e**(math.sin(t) + t)

def difference(method,func,ax = None,verbose = False,method_name=None, plot = False ,param_dict={'color':'red'}):# {{{
    difference = 0
    for i,approx in zip(method[0],method[1]):
        if plot:
            ax.plot([i,i],[approx,func(i)],**param_dict)
        difference = difference + (approx - func(i))**2
    difference = math.sqrt(difference)
    if verbose:
        print('the difference (mid quadratic error) of {} from analytical solution is {}'.format(method_name,difference))
    return difference# }}}

ax.plot(list(np.linspace(interval[0],interval[1],number_of_points)),[func(x) for x in list(np.linspace(interval[0],interval[1],number_of_points))], label='analytické řešení')

difference(euler_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax,param_dict={'label':"Runge-Kutta's method of the fourth degree",'marker':'o','markersize':'2','ls':'--', 'color':'red'}),func,ax=ax,verbose=True,method_name='euler_method',plot=True, param_dict={'color':'red'})
difference(runge_kutts_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax,param_dict={'label':"Runge-Kutta's method of the fourth degree",'marker':'o','markersize':'2','ls':'--', 'color':'purple'}),func,ax=ax,verbose=True,method_name='runge_kutts_method',plot=True, param_dict={'color':'purple'})
ax.legend()
fig.show()
