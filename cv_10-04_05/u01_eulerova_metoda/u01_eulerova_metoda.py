#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

dc_dr = 0.2

f_2017 = 3.2

def euleruv_odhad(h,poc_pod,dy_dx):
    return poc_pod + h * dy_dx

h = float(input('Zadej si h: '))
print(round(euleruv_odhad(h,f_2017,dc_dr),2))

print('(Řešení se v závislosti na kroku h mění lineárně. Čím delší krok máme, tím méně si jsme jistí, že jsme blízko opravdového řešení)')
