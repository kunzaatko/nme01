#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def dNt_dt(t,Nt):
    return (1 + math.cos(t))*Nt

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])
interval = (0,5)

def interval_len(interval):
    return interval[1] - interval[0]

def euler_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':'Eulers method','marker':'o','markersize':'2','ls':'--'}):
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step*difference(t,func[-1]))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)

def mid_point_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':'mid point method','marker':'o','markersize':'2','ls':'--'}):
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step*(difference(t+step/2,func[-1]))/func[-1]*(func[-1]+step/2*difference(t,func[-1])))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)

def heuns_method(difference, init_cond, interval, point_num = 1000, ax = None, plot = False, param_dict = {'label':"Heun's method",'marker':'o','markersize':'2','ls':'--'}):
    """
    *init_cond* must be on the right side of the interval
    """
    func = [init_cond]
    for t in list(np.linspace(interval[0],interval[1],point_num))[1:]: # we already have the first function value as the initial condition
        step = interval_len(interval)/(point_num-2)
        func.append(func[-1]+step/2*(difference(t,func[-1]) + difference(t + step, func[-1] + step*difference(t,func[-1]))))
    if plot:
        ax.plot(list(np.linspace(interval[0],interval[1],point_num)),func, **param_dict)
    return (list(np.linspace(interval[0],interval[1],point_num)),func)

number_of_points = 100
euler_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax)
mid_point_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax)
heuns_method(dNt_dt,1,interval,point_num=number_of_points,plot=True,ax=ax)
ax.legend()
fig.show()
