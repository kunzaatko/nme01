% u04_feedback
% Martin Kunz
% 14 May 2020

# Feedback
@) Změnili byste něco?

Jsem spokojený se cvičením. Bylo by dobré mít více příkladů z praxe, jako byl ten o free fall sedačce.

@) Bylo to lehké/akorát/těžké?

Bylo to tak akorát. Občas jsem strávil docela hodně času na příkladech, ale to bylo i tím, že to nebylo na hodině, protože tam je omezený čas a při jakémkoliv problému se dá obrátit na cvičícího, což distančně nebylo tak pohodlné. Metody nebylo náročné pochopit, prezentace byly dobře udělané, pochopitelné.

@) Jak vám šlo programování v Matlabu/*Pythonu*?

Bylo to překvapivě jednoduché. Ten odkaz na cvičení v pythonu na FJFI mě hodně nastartoval a výhoda u pythonu je, že kdykoliv mám s něčím problém, tak na rozdíl od Matlabu nejsem odkázaný jen na dokumentaci, ale i na stack overflow a na spoustu inspirativního kód baseu na githubu a gitlabu.

@) Chtěli byste raději programovat v jiném jazyce?

Bylo by myslím super, kdyby byl python standardní výukový jazyk. Už je k tomu boře připravený všemi knihovnami a lépe zapadá do většiny programovacích paradigmat, které jsme se stejně učili na základech programování v prváku. Samozřejmě jsem neřešil žádné náročnější úlohy, než, co bylo na cvičeních, takže nevím, jak je na to python vybavený. Bez tohohle cvičení bych se ale v pythonu asi nenaučil, takže možnost používání pythonu jako alternativy hodnotím velmi kladně. Tento článek se mi líbil: [python vs. matlab](https://realpython.com/matlab-vs-python/). Taky je parádní, že se dá jednoduše použít na linuxu a často je dostupný na počítači bez toho, aby o tom uživatel věděl $\implies$ mohl jsem na cvičení použít i jiné zařízení, když jsem zrovna nebyl na bytě a neměl u sebe svůj notebook.

``` python

meth = [secant_meth, interval_half, regula_falsi, tangent_meth]

for n,method in enumerate(meth):
    axes.append(plt.subplot2grid((len(meth),len(meth)+1), (n,len(meth))))

```

Taky se na rozdíl od Matlabu dá najít spoustu dobrých IDE editorů a pluginů na psaní pythonu ve vimu. Nevím, jestli dokáže python něco podobného jako je třeba simulink v matlabu, ale bez takových grafických vymoženin je myslím lepší.

@) Nějaké podněty/nápady/stížnosti/komentáře?

Myslím, že sice byly prezentace skvělé a pochopitelné, ale možná byly až moc. Kdyby si člověk měl možnost *osahat* si nějaký problém/příklad sám, tak by se toho naučil víc. Zároveň by to asi trvalo více času.

Moc díky, za cvičení. Bylo opravdu vidět, že si dáváš záležet a že tomu věnuješ hodně úsilí a času.\
*Martin Kunz*
