#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math
import scipy

def eulers_method(step,init_cond,differential):
    return init_cond[1] + step * differential

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])
def shooting_method(init_cond,border_cond,differential,verbose=True,plot = False, ax = ax ,param_dict={'ls':'--'}, accuracy = 0.001, point_num = 100, over=None, under = None, initial_tries=None):
    if under and over:
        init_under = initial_tries[0]
        init_over = initial_tries[1]
    Xn = list(np.linspace(init_cond[0],border_cond[0],point_num)) # založíme vektor x
    step = (border_cond[0] - init_cond[0])/point_num
    yn_new = [init_cond[1]]
    if initial_tries: # dále se dá na toto navázat a přidat slupku funkce, která si vymyslí vlastní počáteční střely
        vn_new = [(initial_tries[1] + initial_tries[0])/2]
    # nazačátku musíme vystřelit, abychom měli představu
    if (not under) and (not over): # budeme předpokládat, že initial_tries je zadáno rozumně a že tedy opravdu jedna střela poletí nad a jedna pod{{{
        if initial_tries:
            vn_1 = [initial_tries[0]] # nejprve vyřešíme první ze střel
            vn_2 = [initial_tries[1]]
        yn_1 = [init_cond[1]]
        yn_2 = [init_cond[1]]
        for xn in Xn[0:-1]:
            yn_1.append(eulers_method(step,(xn,yn_1[-1]),differential(vn_1[-1]))) # nový odhad
            vn_1.append(eulers_method(step,(xn,vn_1[-1]),-differential(yn_1[-2]))) # nový odhad diferenciálu
        if yn_1[-1] <= border_cond[1]:
            init_under = vn_1[0]
            under = yn_1
        else:
            init_over = vn_1[0]
            over = yn_1
        for xn in Xn[0:-1]:
            yn_2.append(eulers_method(step,(xn,yn_2[-1]),differential(vn_2[-1]))) # nový odhad
            vn_2.append(eulers_method(step,(xn,vn_2[-1]),-differential(yn_2[-2]))) # nový odhad diferenciálu
        if yn_2[-1] <= border_cond[1]:
            init_under = vn_2[0]
            under = yn_2
        else:
            init_over = vn_2[0]
            over = yn_2# }}}
        ax.plot(Xn,under,**param_dict)
        ax.plot(Xn,over,**param_dict)

    for xn in Xn[0:-1]:
        yn_new.append(eulers_method(step,(xn,yn_new[-1]),differential(vn_new[-1]))) # nový odhad
        vn_new.append(eulers_method(step,(xn,vn_new[-1]),-differential(yn_new[-2]))) # nový odhad diferenciálu
    if math.fabs(yn_new[-1]-border_cond[1]) <= accuracy:
        if verbose:
            print("Shooting method finished with initial differential being: {}\nThe resulting function is equal to {} at {} (that is a difference of {} from border condition)".format(vn_new[0],round(yn_new[-1],2),round(border_cond[0],2),(border_cond[1]-yn_new[-1]),2))
        return yn_new
    elif yn_new[-1] < border_cond[1]:
        under = yn_new
        ax.plot(Xn,under,**param_dict)
        shooting_method(init_cond,border_cond,differential,accuracy=accuracy,point_num=point_num,over=over,under=under,initial_tries=(vn_new[0],init_over))
    else:
        over = yn_new
        ax.plot(Xn,over,**param_dict)
        shooting_method(init_cond,border_cond,differential,accuracy=accuracy,point_num=point_num,over=over,under=under,initial_tries=(init_under,vn_new[0]))

def differential(yn):
    return yn


init_cond = (math.pi/4,2*math.sqrt(2))
border_cond = (math.pi/2,2)
point_num = 100

Xn = list(np.linspace(init_cond[0],border_cond[0],point_num)) # založíme vektor x
ax.plot(Xn,[2*math.sin(x) + 2*math.cos(x) for x in Xn],label='Analytické řešení')

shooting_method(init_cond,border_cond,differential,plot=True, ax=ax ,point_num=point_num,initial_tries=[-50,10])

ax.legend()
fig.show()

