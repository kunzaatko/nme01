#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import scipy.integrate
import matplotlib
import math

t = [0,0.50505,1.0101,1.5152,2.0202,2.5253,3.0303,3.5354,4.0404,4.5455,5.0505,5.5556,6.0606,6.5657,7.0707,7.5758,8.0808,8.5859,9.0909,9.596,10.101,10.606,11.111,11.616,12.121,12.626,13.131,13.636,14.141,14.646,15.152,15.657,16.162,16.667,17.172,17.677,18.182,18.687,19.192,19.697,20.202,20.707,21.212,21.717,22.222,22.727,23.232,23.737,24.242,24.747,25.253,25.758,26.263,26.768,27.273,27.778,28.283,28.788,29.293,29.798,30.303,30.808,31.313,31.818,32.323,32.828,33.333,33.838,34.343,34.848,35.354,35.859,36.364,36.869,37.374,37.879,38.384,38.889,39.394,39.899,40.404,40.909,41.414,41.919,42.424,42.929,43.434,43.939,44.444,44.949,45.455,45.96,46.465,46.97,47.475,47.98,48.485,48.99,49.495,50]# {{{}}}
dy_dt = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011172,0.3566,0.7526,1.0051,1.1462,1.8301,2.4327,2.6049,2.3646,2.1659,2.161,2.2791,2.5046,2.6439,2.8603,3.2259,3.7892,4.8366,5.5378,5.6912,5.3922,5.4982,5.4714,5.1098,4.5547,4.6216,4.2198,3.1007,1.4773,0.85489,0.63297,0.60236,0.72444,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,1.9123,0.982,0.36179,0.051684,-2.8767,-8.096,-12.514,-16.132,-18.948,-20.964,-6.6688,-4.6644,-3.3635,-2.1061,-1.1289,-0.76306,-0.60236,-0.63297,-0.7469,-0.75,-0.75,-0.75,-0.86407,-0.98753,-0.82683,-0.37917,-0.0023998,0,0,0,0,0,0,0,0]# {{{}}}

T = np.array(t)
dY_dT = np.array(dy_dt)

velocity_poly = scipy.interpolate.CubicSpline(T,dY_dT)
acceleration_poly = velocity_poly.derivative()
point_num = 10000
time = np.array(np.linspace(0,max(t),point_num))
time_list = list(np.array(np.linspace(0,max(t),point_num)))
height = np.array([velocity_poly.integrate(0,time) for time in time])
velocity = np.array([velocity_poly(time) for time in time])
acceleration = np.array([acceleration_poly(time) for time in time])

# Dalo by se to udělat mnohem jednodušeji pomocí paradigmatu filtru v numpy, ale napadl mě tento "algoritmus", tak jsem si ho naimplementoval. Navíc jsem se ještě pořádně s numpy nenaučil. Rád bych první uměl dobře s pythonem a pak se učil numpy. Potom, co si osvojím i objektovou stranu pythonu...
def intervals_above(func,times,get_above=True):# {{{
    """
    return a list of intervals in the format (a,b) (tuples)
    func := function for which to assert above and below
    times := discrete times for which to assert
    get_above := whether or not to return the above intervals (if False -> returns below intervals)
    """
    result_above = []
    result_below = []
    a = None
    b = None
    above_before = None
    above = None
    for time in times:
        if func(time) >= 0: # interval je above
            above = True # položíme současný stav rovný above
            if above_before == None: # pokud jsme na začátku intervalu
                above_before = False # musíme začít nový interval v dalším kroku
            elif not above_before: # if na_hranici_intervalů
                if not (a == None): # už jsme se pohli z prvního intervalu -> máme interval v paměti
                    b = time_before
                    result_below.append((a,b)) # dostali jsme se na nový interval -> musíme předchozí dát do patřičného seznamu
                a = time # zakládáme nový interval
            above_before = above # předáme štafetu
            time_before = time # předáme štafetu
        else:
            above = False # položíme současný stav rovný below
            if above_before == None: # pokud jsme na začátku intervalu
                above_before = True # musíme začít nový interval v dalším kroku
            elif above_before: # if na_hranici_intervalů
                if not (a == None): # už jsme se pohli z prvního intervalu -> máme interval v paměti
                    b = time_before
                    result_above.append((a,b)) # dostali jsme se na nový interval -> musíme předchozí dát do patřičného seznamu
                a = time # zakládáme nový interval
            above_before = above # předáme štafetu
            time_before = time # předáme štafetu
        b = time_before
    if above_before:
        result_above.append((a,b))
    elif not above_before:
        result_below.append((a,b))
    if get_above:
        return result_above
    else:
        return result_below# }}}

above = intervals_above(acceleration_poly,time_list)
below = intervals_above(acceleration_poly,time_list,get_above=False)

momentum_down = 0
for interval in below:
    momentum_down = momentum_down + scipy.integrate.fixed_quad(acceleration_poly,interval[0],interval[1])[0]

momentum_up = 0
for interval in above:
    momentum_up = momentum_up + scipy.integrate.fixed_quad(acceleration_poly,interval[0],interval[1])[0]

print('Momentum up: {}'.format(momentum_up))
print('Momentum down: {}'.format(momentum_down))
print('Difference: {}'.format(momentum_up+momentum_down))

# Čekal jsem mnohem větší rozdíl (při 1000000 bodů)
# Momentum up: 31.13571880403316
# Momentum down: -29.447259349709647
# Difference: 1.6884594543235139 -> není to nula, protože jsme vycházeli z měřených dat -> nepřesných dat. Navíc nepracujeme s reálnéá funkcí, ale pouze s interpolací naměřených hodnot, takže itegrujeme přes zkonstuovaný model a ne přes reálnou hybnost

