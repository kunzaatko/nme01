#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import matplotlib
import math

t = [0,0.50505,1.0101,1.5152,2.0202,2.5253,3.0303,3.5354,4.0404,4.5455,5.0505,5.5556,6.0606,6.5657,7.0707,7.5758,8.0808,8.5859,9.0909,9.596,10.101,10.606,11.111,11.616,12.121,12.626,13.131,13.636,14.141,14.646,15.152,15.657,16.162,16.667,17.172,17.677,18.182,18.687,19.192,19.697,20.202,20.707,21.212,21.717,22.222,22.727,23.232,23.737,24.242,24.747,25.253,25.758,26.263,26.768,27.273,27.778,28.283,28.788,29.293,29.798,30.303,30.808,31.313,31.818,32.323,32.828,33.333,33.838,34.343,34.848,35.354,35.859,36.364,36.869,37.374,37.879,38.384,38.889,39.394,39.899,40.404,40.909,41.414,41.919,42.424,42.929,43.434,43.939,44.444,44.949,45.455,45.96,46.465,46.97,47.475,47.98,48.485,48.99,49.495,50]# {{{}}}
dy_dt = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011172,0.3566,0.7526,1.0051,1.1462,1.8301,2.4327,2.6049,2.3646,2.1659,2.161,2.2791,2.5046,2.6439,2.8603,3.2259,3.7892,4.8366,5.5378,5.6912,5.3922,5.4982,5.4714,5.1098,4.5547,4.6216,4.2198,3.1007,1.4773,0.85489,0.63297,0.60236,0.72444,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,1.9123,0.982,0.36179,0.051684,-2.8767,-8.096,-12.514,-16.132,-18.948,-20.964,-6.6688,-4.6644,-3.3635,-2.1061,-1.1289,-0.76306,-0.60236,-0.63297,-0.7469,-0.75,-0.75,-0.75,-0.86407,-0.98753,-0.82683,-0.37917,-0.0023998,0,0,0,0,0,0,0,0]# {{{}}}

T = np.array(t)
dY_dT = np.array(dy_dt)

velocity_poly = scipy.interpolate.CubicSpline(T,dY_dT)
acceleration_poly = velocity_poly.derivative()
point_num = 10000
time = np.array(np.linspace(0,max(t),point_num))
height = np.array([velocity_poly.integrate(0,time) for time in time])
velocity = np.array([velocity_poly(time) for time in time])
acceleration = np.array([acceleration_poly(time) for time in time])

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])
ax.plot(time,velocity,label='velocity')
ax.plot(time,acceleration,label='acceleration')
ax.plot(time,height,label='height')
print(r'Maximum height: {} m'.format(round(np.max(height),1)))
ax.legend(loc=2)
ax.grid()
ax.set_xlabel('čas (s)')
ax.set_ylabel(r'height ($m$), velocity ($m/s$), acceleration ($m/s^2$)')
ax.annotate(r'Maximum height is: {} $m$'.format(round(np.amax(height),1)), xy=(float(time[np.unravel_index(height.argmax(),height.shape)[0]]),np.amax(height)+1.5))

for ind,acceleration_alt in enumerate(acceleration[np.unravel_index(height.argmax(),height.shape)[0]:]):
    if acceleration_alt > 0:
        end_of_free_fall_ind = np.unravel_index(height.argmax(),height.shape)[0]+ind
        end_of_free_fall = time[end_of_free_fall_ind]
        break

free_fall_timespan = end_of_free_fall-time[np.unravel_index(height.argmax(),height.shape)[0]]
print(r'Time in free fall was: {} s'.format(round(free_fall_timespan,2)))
ax.annotate(r'Time in free fall was: {} $s$'.format(round(free_fall_timespan,2)), xy = (float(time[end_of_free_fall_ind]),float(velocity[end_of_free_fall_ind])-2))
print(r'Maximum acceleration is: {} g'.format(round(np.amax(acceleration/9.8),2)))
ax.annotate(r'Maximum acceleration is: {} g'.format(round(np.amax(acceleration/9.8),2)), xy = (time[np.unravel_index(acceleration.argmax(),height.shape)[0]],np.amax(acceleration) + 2 ))
fig.show()
