A=[5,1,1;2,7,3;2,3,8]
b=[1;1;1];
L=A-triu(A,0);
U=A-tril(A,0);
D=A-L-U;

%x_0 = [1;2;3];
x_nj = [1;2;3];
x_ng = x_nj;

for i=1:100 % Jakobiho x_1 = Gaussova x_1
    x_nj = inv(D)*(b-(L+U)*x_nj);
    x_ng = inv(D+L)*(b-U*x_ng);

    %x_nj = (b-(L+U)*x_nj)\D % TODO: Jaktože tohle nefunguje?
end
x_nj
x_ng
x_matlab=A\b

diff_x_nj = x_matlab - x_nj
diff_x_ng = x_matlab - x_ng
