% Vypocet nejvetsiho vlastniho cisla

A = [3 -1; 2 0];                                % Definice matice
v = [1;3];                                      % Uvodni odhad vlastniho vektoru

iteraci = 50;                                   % Pocet iteraci
vlCislo = zeros(1,iteraci);                     % Inicializace vlastniho cisla pro kazdou iteraci

for i=1:iteraci   
	vlCislo(i) = norm(A*v);                     % Aproximace vlastni cisla v i-tem kroku
	v = A*v/norm(A*v);                          % Aproximace vlastni vektoru v i-tem kroku - musíme podělit velikostí odhadu vlastního čísla, jinak se bude výsledek jen zvětšovat.
end

lambda = norm(A*v);                             % Hledane nejvetsi vlastni cislo

disp('Nejvetsi vlastni cislo:');
disp(lambda);
disp('A k nemu vlastni vektor:');
disp(v);

plot(1:iteraci,vlCislo);                        % Graf jak jsme se priblizovali k vlastnimu cislu
xlabel('Iterace');
ylabel('Vlastni cislo \lambda')
