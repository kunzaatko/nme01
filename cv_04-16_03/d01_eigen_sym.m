%% Vlastni cisla pocitane symbolicky

syms lambda                                         % symbolicka promena

A   = [3-lambda -1; 2  -lambda];                    % pokud pouzijeme symbolickou promenou, pak i matice je symbolicka

det(A);                                             % determinant symbolicky                                          

vl_cisla   = solve(det(A),lambda);                  % Resime det(A)=0 

A1  = subs(A,vl_cisla(1));                          % Za lambda v matici A nahradime prvni vlastni cislo vl_cisla(1)
A2  = subs(A,vl_cisla(2));                          % Za lambda v matici A nahradime druhe vlastni cislo vl_cisla(2)

vl_vektor1 = null(A1);                              % Pro ziskani prvniho vl. vektoru resime A1 = 0
vl_vektor2 = null(A2);                              % Pro ziskani druheho vl. vektoru resime A2 = 0