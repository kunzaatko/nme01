% Srovnani Jacobiho metody a Gauss-Seidelovy metody
% pro dve ruzne matice A a B

A   = [5  1 1; 2 7 3; 2 3 8];                       % Matice soustavy A
B   = [5 10 1; 2 7 3; 2 3 8];                       % Matice soustavy B

b   = [1;1;1];                                      % Prava strana

x0_Jacobi   = [1;2;3];                              % Odhad reseni
x0_GS       = [1;2;3];

iteraci=100;                                        % Pocet iteraci

reseniA     = A\b;                                  % Reseni primou metodou
reseniB     = B\b;

L           = tril(A,-1);                           % Prvky matice A pod diagonalou                
U           = triu(A,1);                            % Prvky matice A nad diagonalou
D           = A-L-U;                                % Diagonala matice A

xJ          = x0_Jacobi;                            % Jako docasne reseni zvolime odhad
xGS         = x0_GS;

for i=1:iteraci                                     % Vypocet pro matici A
	chybaAJ(i)   = norm(xJ-reseniA);                % "vzdalenost" reseni v i-tem kroku 
    chybaAGS(i)  = norm(xGS-reseniA);               % od skutecneho reseni (prime metody)
	xJ           = inv(D)*(b-(L+U)*xJ);              % Jacobiho metoda             
	xGS          = inv(D+L)*(b-U*xGS);           % Gauss-Siedelova metoda
end

L           = tril(B,-1);                           % Prvky matice B pod diagonalou                
U           = triu(B,1);                            % Prvky matice B nad diagonalou
D           = B-L-U;                                % Diagonala matice B

xJ          = x0_Jacobi;                            % Jako docasne reseni zvolime odhad
xGS         = x0_GS;

for i=1:iteraci                                     
	chybaBJ(i)   = norm(xJ-reseniB);                % Stejna iterace jako vyse, ale
    chybaBGS(i)  = norm(xGS-reseniB);               % pro matici B
	xJ           = inv(D)*(b-(L+U)*xJ);                       
	xGS          = inv(D+L)*(b-U*xGS);       
end

subplot(211);
plot(1:iteraci,chybaAJ,1:iteraci,chybaBJ);
title('Jacobi: odchylka od skutecneho reseni');
xlabel('Pocet iteraci');
ylabel('Odchylka');
legend('matice A','matice B');
set(gca,'YScale','log');

subplot(212);
plot(1:iteraci,chybaAGS,1:iteraci,chybaBGS);
title('Gauss-Seidel: odchylka od skutecneho reseni');
xlabel('Pocet iteraci');
ylabel('Odchylka');
legend('matice A','matice B');
set(gca,'YScale','log');

% Jakou z těchto dvou metod by sis vybral(a) v případě řešení 
% první soustavy Ax=b a jakou v případu řešení druhé soustavy Bx=b?
% Odpověz:
% V obou případech je výhodnější použít Gaussovu-Seidlovu metodu, protože
% pro oba příklady konverguje rychleji (přičemž v u matice B je dokonce
% Jakobiho metoda nevhodná pro spočítání řešení vůbec, protože odchylka je
% rostoucí fce)
%

