#! /bin/python

# _*_ coding: utf-8 _*_
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math

# Generování Čebyševových polynomů {{{
a = 1
b = 100
c = 0.001
t = np.arange(a,b+c,c)

print(r"$$ t \in <a,b> \mapsto x \in <-1,1>$$ ")
print("t = {}".format(t))

x = t
for i,ti in enumerate(t):
    x[i] = (2*ti-(a+b))/(b-a)
# nyní x je z intervalu <-1,1>

print("x = {}".format(x))

# funkcionál, který vrací z předchozích dvou polynomů další polynom
def cebysevuv_poly_n_plus_one (cebysevuv_poly_n, cebysevuv_poly_n_minus_one): #  je hodně hustý, že tohle python umožňuje
    return lambda x: 2*x*cebysevuv_poly_n(x) - cebysevuv_poly_n_minus_one(x)

def cebysevuv_poly_0(x):
    return np.ones(np.shape(x))

def cebysevuv_poly_1(x):
    return x

# počáteční polynomy, ze kterých se bude iterovat
cebysevovy_poly = [cebysevuv_poly_0, cebysevuv_poly_1]

print('Zadej počet Čebyševových polynomů, které chceš vykreslit:')
max_n = int(input())
for n in range(2,max_n+1): # generování polynomů do stupně 4
    cebysevovy_poly.append(cebysevuv_poly_n_plus_one(cebysevovy_poly[n-1],cebysevovy_poly[n-2]))# }}}

# Vykreslení grafů {{{

cebysevovy_poly_fig = plt.figure()
cebysevovy_poly_axes = cebysevovy_poly_fig.add_axes([0.05,0.05,0.9,0.9])
cebysevovy_poly_axes.set_xlabel('x')
cebysevovy_poly_axes.set_ylabel('y')
cebysevovy_poly_axes.set_title('Čebyševovy polynomy')
for nth, polynom in enumerate(cebysevovy_poly): # za kazdý polynom v seznamu vykresli
    cebysevovy_poly_axes.plot(x, polynom(x), label="$U_{}$".format(nth))

cebysevovy_poly_axes.legend(loc=2)
cebysevovy_poly_fig.show()

# }}}
