# Aproximace

## Čebyševova úloha
+ hledá se nejlepší _stejnoměrná_ aproximace funkce $f(x)$ na intervalu
+ funkce $h(x)$ - na $<a,b>$ minimalizuje
    $$\min (\max_{x \in <a,b>} \|f(x) - h(x)\| )$$
    - __?!__ nemělo by to být jako při supremálním kritériu - vyjde to stejně?
        $$\min (\sup_{x \in <a,b>} \|f(x) - h(x)\| )$$
+ minimax:
:  polynom nejlepší stejnoměrné aproximace _(špatně se konstruuje)_
+ konstrukce pomocí _Čebyševových polynomů_ _(lehce se konstruuje a je skoro tak přesná)_

### Čebyševovy polynomy
+ Lineánrní transformace do definičního oboru funkce $\cos$: \begin{align}t \in [ a,b ] \mapsto x \in [ -1,1 ]\implies\\
      x = \frac{2t-(a+b)}{b-a}\end{align}
+ Čebyševův polynom:
:      $T_n(x) = \cos(n \arccos x)$

+ rekurentní vzorec:
:      $T_{n+1}(x) = 2xT_n(x) - T_{n-1}(x)$

