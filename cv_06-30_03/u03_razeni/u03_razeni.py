#! /bin/python

import numpy as np
import timeit
import math
import matplotlib
import matplotlib.pyplot as plt
import re

# _to_sort = [5, 6, 2, 9, 8, 7, 4, 1, 3]
# to_sort = []

def sort_sel(to_sort): # ✓ - ten jsem napsal sám {{{
    index = 0 # přes tento index se posouváme v rámci seznamu na konec
    while to_sort[index] is not to_sort[-1]: # postupné posouvání indexu
        (minimum_index, minimum) = (index, to_sort[index])  # inicializace minima, které hledáme jako bod, který právě zaměňujeme
        ind = index # přes tenhle index budeme od zaměňovaného uzlu hledat minimum
        for comp in to_sort[index:]: #  posouváme se od zaměňovaného uzlu nakonec
            if comp < minimum: # pokud máme menší prvek
                (minimum_index, minimum) = (ind, comp) # změň současnou verzi hledaného minima
            ind += 1 # pokud ne, posuň se dál
        to_sort[minimum_index] = to_sort[index]  # posuň prvek na místo minima
        to_sort[index] = minimum # prošli jsme celý zbytek seznamu -> menší už není -> zaměň prvky
        index += 1 # posuň se na další prvek
    return to_sort # vrať setřízený seznam
# }}}

def sort_ins(to_sort): # ✓ - ten jsem napsal sám {{{
    ind = 1  # statický index
    while to_sort[ind-1] is not to_sort[-1]: # posouváme se doprava
        prev_inds = ind-1 # budeme iterovat přes předcházející prvky -> posuvný index
        while prev_inds != -1: # dokud nejsme na začátku seznamu
            if to_sort[prev_inds] <= to_sort[ind]: # pokud prvek pod posuvným indexem je menší nebo rovný
                to_sort.insert(prev_inds+1, to_sort[ind]) # vlož za posuvný index
                del to_sort[ind+1] # smaž statický prvek tam, kde byl
                break # našli jsme jeho správné místo
            elif prev_inds == 0 and to_sort[0] >= to_sort[ind]: # pokud jsem na začátku seznamu a stále jsem jeho místo nenašli -> patří na začátek, nebo tam kde je => ověřujeme, jestli patří na začátek
                to_sort.insert(0, to_sort[ind]) # vlož ho na začátek
                del to_sort[ind+1] # vymaž ho na svém místě
            prev_inds -= 1 # posuň se doleva
        ind += 1 # posuň se na další statický index

# }}}

def sort_bub(to_sort): # ✓ - ten jsem napsal sám {{{
    sorted = False # ještě není setřízený
    while sorted == False: # dokud není setřízený
        sorted = True # předpokládáme, že je setřízený
        ind = 0 # budeme se posouvat od prvního prvku
        while not to_sort[ind] is to_sort[-1]: # dokud nejsme na posledním místě
            if to_sort[ind] > to_sort[ind+1]: # dva vedlejší prvky nejsou ve správném pořadí
                c = to_sort[ind] # pomocná proměnná
                to_sort[ind] = to_sort[ind+1] # prohazujeme hodnoty
                to_sort[ind+1] = c # vkládáme hodnotu z pomocné proměnné
                sorted = False # ještě stále nacházíš neprohozené sousedy -> seznam není setřízený
            ind += 1 # posuň se na další index
    return to_sort # vrať setřízený seznam
# }}}

# sort_heap prevzato z geeksforgeeks - proč nedodržují pep8?! {{{
# Python program for implementation of heap Sort

# To heapify subtree rooted at index i.
# n is size of heap
def heapify(arr, n, i):
    largest = i # Initialize largest as root
    l = 2 * i + 1     # left = 2*i + 1
    r = 2 * i + 2     # right = 2*i + 2

    # See if left child of root exists and is
    # greater than root
    if l < n and arr[i] < arr[l]:
        largest = l

    # See if right child of root exists and is
    # greater than root
    if r < n and arr[largest] < arr[r]:
        largest = r

    # Change root, if needed
    if largest != i:
        arr[i],arr[largest] = arr[largest],arr[i] # swap

        # Heapify the root.
        heapify(arr, n, largest)

# The main function to sort an array of given size
def sort_heap(arr):
    n = len(arr)

    # Build a maxheap.
    for i in range(int(n/2 - 1), -1, -1):
        heapify(arr, n, i)

    # One by one extract elements
    for i in range(n-1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i] # swap
        heapify(arr, i, 0)
    return arr
# }}}

# sort_quick prevzato z geeksforgeeks {{{
#  def sort_quick(to_sort):
    #  """TODO: Docstring for sort_quick.

    #  :function: TODO
    #  :returns: TODO

    #  """
    #  sorted = False
    #  while sorted == False:
        #  sorted = True
        #  (pivot_index, pivot) = (0, to_sort[0])
        #  store_index = pivot_index + 1
        #  for comp in to_sort[pivot_index + 1:]:
            #  if comp < pivot:
    #  # Swap comp with store_index->element {{{
                #  c = comp
                #  comp = to_sort[store_index]
                #  to_sort[store_index] = c
    #  # }}}
                #  store_index += 1
        #  c = pivot
        #  pivot_index, pivot = store_index, to_sort[store_index-1]
        #  to_sort[store_index] = c
    #  return to_sort
    # ^ můj pokus

def partition(arr,low,high):
    i = (low-1)         # index of smaller element
    pivot = arr[high]   # pivot

    for j in range(low, high):
        if arr[j] < pivot:
            i = i+1
            arr[i],arr[j] = arr[j],arr[i]
    arr[i+1],arr[high] = arr[high],arr[i+1]
    return ( i+1 )

def sort_quick(arr, low=0, high = -1):
    if high == -1:
        high = len(arr)-1
    if low < high:
        pi = partition(arr,low,high)

        sort_quick(arr,low,pi-1)
        sort_quick(arr, pi+1, high)
    return arr
# }}}


#  ┌────────────────────────────────────┐
#  │ srovnávání časové náročnosti metod │
#  └────────────────────────────────────┘

'''
Toto už je moje dobrovolná práce. Snažil jsem se sám zkusit ověřit, že jsou
metody, tak náročné, jako podle teorie a trochu si osvojit programování v
pythonu. Čas asi ale nereflektuje dost přesně náročnost. Potřeboval bych
počítač, který třízení neovlivní výkonostně tolik, aby byl při pozdějších
třízeních pomalejší. Pro větší seznamy se přesáhl limit na počet rekurzí u
quick sortu. Mimochodem, hodně mě baví generátorová notace v pythonu... :). Je
dobrá změna pracovat s vlastními daty. Je zajímavé sledovat, jak quicksort je
pro různé seznamy jinak rychlý. V některých případech byl i rychlejší, než
heap.
'''


sorts = [sort_sel, sort_ins, sort_bub, sort_heap, sort_quick]
number_of_elements = [int(n) for n in np.linspace(1,3000,100)]
lists_to_sort = [np.random.rand(n).tolist() for n in number_of_elements]
times = [[timeit.timeit(lambda: sorts[n](lists_to_sort[i].copy()), number =
                                         3) for i in range(0,
                                                           len(lists_to_sort))]
         for n in range(0,len(sorts))]

x = number_of_elements
y = times
fig = plt.figure()
axes_time_compare = fig.add_axes([0.1,0.1,0.8,0.8])
axes_time_compare.set_title('Srovnání rychlostí funkcí sort:')
axes_time_compare.set_ylabel('čas v ms')
axes_time_compare.set_xlabel('počet prvků v seznamu')
for n,sort in enumerate([re.split(r'\s', str(sorts[n]).strip('<function '))[0] for n in range(0,len(sorts))]):
    axes_time_compare.plot(x,y[n],label=f'{sort}')

axes_time_compare.legend(loc=2)
axes_time_compare.grid()
fig.show()

