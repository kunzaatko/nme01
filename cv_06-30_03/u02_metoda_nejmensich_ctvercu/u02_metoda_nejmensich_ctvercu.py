#! /bin/python

# _*_ coding: utf-8 _*_
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math

# Metoda čtverců: {{{
data_set = []
print('Zadej naměřená data (pro defaultní data nezadávej nic):')
while True:
    print('x',end='->')
    x = input()
    if x == '':
        break
    else:
        x = float(x)
    print('\n', 'y', sep='', end='->')
    y = input()
    if y == '':
        break
    else:
        y = float(y)
    point = [x,y]
    print(list(point))
    data_set.append(list(point))
if data_set == []:
    data_set = [[0,5],[1,3],[3,3],[5,2],[6,1]]
    #  data_set = np.random.rand(9000,2)

data_set = np.array(data_set, dtype=float)

c = np.dot(data_set[:,0],data_set[:,1]) - (data_set[:,0].sum()*data_set[:,1].sum())/data_set[:,1].size
d = (data_set[:,0] ** 2).sum() - ((data_set[:,0].sum()) ** 2)/data_set[:,0].size

a = c/d
b = (np.dot(data_set[:,1],data_set[:,0]) - a * (data_set[:,0] **
                                                2).sum())/data_set[:,0].sum()
# }}}

# Ploting {{{
fig = plt.figure()
plot1 = fig.add_axes([0.1,0.1,0.8,0.8])
plot1.set_title("Metoda čtverců:")
plot1.plot(data_set[:,0],data_set[:,1],marker='x', ls='')
x = np.linspace(data_set.min(),data_set.max(),200)
plot1.plot(x,a*x+b,label=f"y = {a: .3f}x + {b: .3f}")
plot1.legend(loc=1)
fig.show()
# }}}
