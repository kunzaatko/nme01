#! /bin/python

import numpy as np
import matplotlib.pylab as plt
import matplotlib
import math
import re

def f(x):
    return 2 * math.sqrt(x) - 3

f_num = np.vectorize(f)


def secant_meth(fun, x0, x1, iternum=1000, return_tries=False, tries=None):# {{{
    n = 0
    pts = [ x0, x1 ]
    while n <= iternum:
        xn = (pts[-2]*fun(pts[-1]) - pts[-1]*fun(pts[-2]))/(fun(pts[-1])- fun(pts[-2]))
        pts.append(xn)
        if return_tries == True:
            tries.append(xn)
        if abs(fun(xn)) > 0.00001:
            print(f'Escaped loop after {n} iterations with the result {xn}')
            break
        n += 1
    print(f'Escaped loop after {iternum} iterations with the result {xn}')
    return pts[-1]# }}}

def interval_half(fun, a, b, iternum=1000, return_tries=False, tries=None):# {{{
    n = 0
    while n <= iternum:
        pt = (a + b)/2
        if return_tries == True:
            tries.append(pt)
        print(f'Trying point {pt}')
        if fun(a)*fun(pt) > 0 and abs(fun(pt)) > 0.00001:
            print(f"f({a})*f({pt}) > 0 -> a = {pt}")
            a = pt
        elif fun(b)*fun(pt) > 0 and abs(fun(pt)) > 0.00001:
            print(f"f({b})*f({pt}) > 0 -> b = {pt}")
            b = pt
        else:
            print(f'Escaped loop after {n} iterations with the result {pt}')
            return pt
        n += 1
    print(f'Escaped loop after {iternum} iterations with the result {pt}')
    return pt# }}}

def regula_falsi(fun, a, b, iternum=1000, return_tries=False, tries=None):# {{{
    n = 0
    while n <= iternum:
        avg_der_fun = (fun(a)-fun(b))/(a+b)
        pt = a + fun(a)/avg_der_fun
        if return_tries == True:
            tries.append(pt)
        print(f'Trying point {pt}')
        if fun(a)*fun(pt) > 0 and abs(fun(pt)) > 0.00001:
            print(f"f({a})*f({pt}) > 0 -> a = {pt}")
            a = pt
        elif fun(b)*fun(pt) > 0 and abs(fun(pt)) > 0.00001:
            print(f"f({b})*f({pt}) > 0 -> b = {pt}")
            b = pt
        else:
            print(f'Escaped loop after {n} iterations with the result {pt}')
            return pt
        n += 1
    print(f'Escaped loop after {iternum} iterations with the result {pt}')
    return pt# }}}

def tangent_meth(fun, x0, iternum=1000, return_tries=False, tries=None):# {{{
    n = 0
    xn = x0
    while n <= iternum:
        der_fun = (fun(xn+0.00001)-fun(xn))/(0.00001)
        xn = xn - fun(xn)/der_fun
        if return_tries == True:
            tries.append(xn)
        if abs(fun(xn)) < 0.00001:
            print(f'Escaped loop after {n} iterations with the result {xn}')
            return xn
        n += 1
    print(f'Escaped loop after {iternum} iterations with the result {xn}')
    return xn # }}}

a, b = 0, 10

meth = [secant_meth, interval_half, regula_falsi, tangent_meth]
fig = plt.figure()
axes = []
tries = []
x = np.linspace(a,b,10000)

for n,method in enumerate(meth):
    axes.append(plt.subplot2grid((len(meth),len(meth)+1), (n,len(meth))))
    tries.append([])
    print(repr(method(f, a, b, return_tries=True, tries=tries[n])))
    axes[n].set_title(re.split(r'\s', repr(method).strip(r'<function'))[1],
                      loc='left')
    axes[n].plot(x,f_num(x))
    axes[n].plot(tries[n], f_num(tries[n]), marker='o', color='blue', ls='',
                 label='generované body')
    axes[n].plot(tries[n], [0 for tri in tries[n]], marker='x', color='red', ls='')
    axes[n].grid(True)
    # axes[n].legend(loc=2)

res = 2.25

comp = plt.subplot2grid((len(meth),len(meth)+1), (0,0), rowspan=len(meth),
                        colspan=len(meth))
comp.grid(True)
comp.set_title('Srovnání metod')
tries_same_len = tries
for tri in tries_same_len:
    for tri_new in range(max([len(tri1) for tri1 in tries])-len(tri)):
        tri.append(res)

for n,meth in enumerate(meth):
    comp.plot([i+1 for i in range(max([len(tri) for tri in
                                       tries]))],[tries_same_len[n][o]-res for o in
                                                  range(max([len(tri1) for tri1
                                                             in tries]))],
              label=re.split(r'\s', str(meth).strip(r'<function'))[1])
comp.legend()
fig.tight_layout()

fig.show()
