#! /bin/python

import numpy as np
import matplotlib.pylab as plt
import matplotlib
import math

def f(x):
    return 2 * math.sqrt(x) - 3

f_num = np.vectorize(f)

tries = []
def regula_falsi(fun, a, b, iternum=1000, return_tries=False, tries=None):
    n = 0
    while n <= iternum:
        avg_der_fun = (fun(a)-fun(b))/(a+b)
        pt = a + fun(a)/avg_der_fun
        if return_tries == True:
            tries.append(pt)
        print(f'Trying point {pt}')
        if fun(a)*fun(pt) > 0:
            print(f"f({a})*f({pt}) > 0 -> a = {pt}")
            a = pt
        elif fun(b)*fun(pt) > 0:
            print(f"f({b})*f({pt}) > 0 -> b = {pt}")
            b = pt
        else:
            print(f'Escaped loop after {n} iterations')
            return pt
        n += 1
    return pt

print(regula_falsi(f, 0, 10, return_tries=True, tries=tries))

fig = plt.figure()
axes = fig.add_axes([0.1,0.1,0.8,0.8])
x = np.linspace(0,10,10000)
axes.plot(x,f_num(x))
axes.plot(tries, f_num(tries), marker='o', color='blue', ls='')
axes.plot(tries, [0 for tri in tries], marker='x', color='red', ls='')
axes.grid(True)
fig.show()
