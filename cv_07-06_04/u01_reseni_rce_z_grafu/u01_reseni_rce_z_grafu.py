#! /bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math

def f(x):
    return 2*math.sqrt(x)-3
f_num = np.vectorize(f)


fig = plt.figure()
graph = fig.add_axes([0.1,0.1,0.8,0.8])
graph.set_xlabel('x')
graph.set_ylabel('y')

points = 1000
x = np.linspace(0,10,points)
y = f_num(x)
graph.plot(x,y)

y = np.zeros(points)
graph.plot(x,y)

graph.grid(True)
fig.show()
