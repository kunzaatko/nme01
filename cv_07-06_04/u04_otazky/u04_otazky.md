# Otázky

1. Jakým způsobem reprezentujeme čísla v počítači?

Binárně podle typu. Zajímavější, než jak se čísla reprezentují v závislosti na typu je reálná reprezentace celých binárních čísel, jakožto matčinných reprezenatcí všech ostatních jako binární pole. Nemůžeme totiž je reprezentovat jednoduše, tak jako bychom to udělali třeba v matematice, ale musí v řadě nul a jedniček být "escape značky", které označí, kde číslo začíná a kde končí, případně, kdy už se nejedná o reprezentaci čísla, ale o Turingův algoritmus... Toho se docílí tím, že se jedničky a nuly přepisují na 01 a 00 popořadě. To znamená, že nuly a jedničky reprezentujeme podle počtu jedniček mezi dvěma nulami. Další operace se potom dají reprezentovat větším počtem jedniček mezi dvěma nulami. [The Emperor's New Mind - Roger Penrose] (nikdy moc nevím, jak přesně bych měl dělat citace...)

2. Jaký je rozdíl mezi absolutní a relativní chybou?

Absolutní chyba není závislá na velikosti výsledku. To znamená, že je reprezentativní nezávisle na výsledku např. $\Delta = 1 \text(km)$ na poloměru oběhu země kolem slunce ($\delta \dot{=} 7\times 10^{-7}%$) oproti stejné absolutní chybě na dráze formule 1 (asi 3 km) jsou desítky procent a není ani trochu zanedbatelná. Liší se také v tom, jak se s nimi počítá. Při násobení nás zajímá $\delta$ a při sčítání $\Delta$.

3. Jaký je rozdíl mezi matematickým modelem a numerickým modelem?

Numerický model je číselný $\implies$ přibližný a matematický je rigorózní $\implies$ vždy přesný. Numericky je ale možné spočítat téměř všechno, naproti tomu, při každém matematickém modelu (soustavě axiomů) budou existovat tvrzení, která budou pravdivá, ale nedokazatelná.

4. Vyjmenuj alespoň dvě přímé metody řešení soustav lineárních rovnic a dvě iterační metody řešení soustav lineárních rovnic.

přímé:
: metody:

* Gaussova eliminace
* LU rozklad


iterační:
: metody:

* Jacobiho
* Gauss-Seidlova

5. Co hledáme v případě částečného problému vlastních čísel?

Největší vlastní číslo (v absolutní hodnotě tj. takové které zobrazení vektoru při změně vektoru nejvíce ovlivňuje.).

6. K čemu v Matlabu slouží funkce interp1, na co bys ji použil(a)?

Aproximaci úsečkami, které jsou v bodech rovny naměřeným datům. Použil bych jí, abych si dokázal představit, jaká by k situaci mohlo být teoretické řešení.

7. Máš dva algoritmy, jeden se složitostí $O(n\log(n))$ a druhý s $O(n^2)$, jaký by sis vybral?

To není obecně jednoznačné. Pro malý počet dat může být algoritmus se složitostí $O(n^2)$ lepší, ale pro větší už naopak. Zpravidla použiju algoritmus složitosti $O(n\log(n))$, protože, pokud výpočet bude mít málo dat, bude krátký nezávisle na vybraném algoritmu a pokud ho nebudu chtít mnohokrát opakovat, je mi jedno, že bych to s jiným zvládl rychleji.

8. Už ses se díval na tyto stránky? http://kfe.fjfi.cvut.cz/~nme/

Ano. Trošičku. Fakt...
