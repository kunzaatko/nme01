load('u04_spline.dat')
x = spline(:,1);
y = spline(:,2);
n = 2000;                                    % počet bodů vyhodnocení na interpolační křivky
x_n = linspace(min(x),max(x),n);
y_n_spline = interp1(x,y,x_n,"spline");
y_n_linear = interp1(x,y,x_n,"linear");
y_n_cubic = interp1(x,y,x_n,"PCHIP");
hold on
plot(x_n,y_n_spline,"b--", 'LineWidth', 3)
plot(x_n,y_n_linear,"r:", 'LineWidth', 3)
plot(x_n,y_n_cubic,"c-.", 'LineWidth', 3)
plot(x, y, "rx",'LineWidth',3 ,'MarkerSize', 20)
legend('interpolační spline','lineární interpolace','kubická interpolace','naměřené body','Location','NorthEast', 'FontSize', 20)
title('Approximace naměřených dat různými způsoby','FontSize',20)
