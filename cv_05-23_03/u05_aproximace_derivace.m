% NME 5 - Aproximace derivace

function AppDer

    function f = f(x)
        f = sin(x); % zadana funkce
    end

    function df = df(x)
        df = cos(x); % zname analyticke vyjadreni derivace
    end

    function df = df_n1(x,h) % odhad derivace metodou prvniho radu
        df = ( f(x+h)-f(x) ) / h;
    end

    function df = df_n2(x,h) % odhad derivace metodou druheho radu
        df = ( f(x+h/2)-f(x-h/2) ) / h;
    end

%%%%%%%% ZDE PRIDEJ funkci df = df_n4(x,h) odhadu derivace metodou ctvrteho radu %%%%%%%%

    function df = df_n4(x,h) % odhad derivace metodou čtvrtého řádu
        df = ( f(x-2*h)+8*f(x+h)-8*f(x-h)-f(x+2*h) ) / (12*h);
    end

h = 0.1;                        % krok
x = 0:0.1:2*pi;                 % body ve kterych pocitame derivaci

plot(x,f(x));                   % puvodni funkce
title('Puvodni funkce');
waitforbuttonpress;
plot(x, df(x),'LineWidth',3);                 % analyticka derivace
title('Derivace');
hold on
legend('Analytická derivace','FontSize',20)
waitforbuttonpress;
plot(x, df_n1(x,h),'b--','LineWidth',3);       % odhad derivace metodou prvniho radu
legend('Analytická derivace','Aproximace prvního řádu','FontSize',20)
waitforbuttonpress;
plot(x, df_n2(x,h), 'r-.','LineWidth',3);      % odhad derivace metodou druheho radu
legend('Analytická derivace', 'Aproximace prvního řádu', 'Aproximace druhého řádu (centrální)', 'FontSize', 20)

%%%%%%%% ZDE PRIDEJ plot odhadu derivace metodou ctvrteho radu %%%%%%%%
waitforbuttonpress;
plot(x, df_n4(x,h), 'g:','LineWidth',3);
legend('Analytická derivace', 'Aproximace prvního řádu', 'Aproximace druhého řádu (centrální)', 'Aproximace čtvrtého řádu', 'FontSize', 20)

end
