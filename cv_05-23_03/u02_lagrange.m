function Lagrange()
% NME 5 - Lagrangeuv polynom

    function y = funkce(x)                                          % funkce, kterou budeme interpolovat
%        y = 5*x.^5-x.^4+3*x.^2-11*x+7;                            % fce 1
%        y = x.^4 .* log(10+abs(x)) .* cos(x) .* exp(-abs(x));     % fce 2
        y = exp(-x).*sqrt(abs(x));                                % fce 3
%        y = 1./(abs(x)+1).*sin(x);                                % fce 4
%        y = 1./(1+25*x.^2);                                       % fce 5
    end

    function y = Lagr(x1, x2, v, ii)
        y=0;
        for kk = 1 : ii
            index = find(x2 ~= x2(kk));                             % vyberu vsechny indexy mimo aktualniho kk
            % x1        - bod ve kterem interpoluji
            % x2        - body, ve kterych funkci zname
            % x2(index) - vsechny body mimo x2(kk)
            % v(kk)     - hodnota v aktualnim bode x2(kk)
            % ii-1      - stupen polynomu
            y = y + v(kk) * prod(x1-x2(index)) / prod(x2(kk)-x2(index));
        end
    end


a  = -10;                                                           % minimum x
b  =  10;                                                           % maximum x
N  = 1000;                                                          % Pocet bodu, pro interpolaci
cx =  a : (b-a)/N : b;                                              % body, ve kterych budeme funkci interpolovat
n  =  21;                                                           % nejvyssi stupen interpolace
figure;

for i = 1 : n                                                       % zkousime ruzne stupne interpolacniho polynomu
	step=(b-a)/(i+1);                                               % velikost kroku pro dany stupen interpolace

    xx=zeros(1,i);                                                  % body, ve kterych funkci zname
    val=zeros(1,i);                                                 % hodnoty funkce ve znamych bodech (namerene hodnoty)
    for j = 1 : i                                                   % presne funkci vyhodnocujeme v i bodech
        xx(j)  = a + j*step;                                        % ekvidistantni body
        val(j) = funkce(xx(j));
    end

    eL=zeros(1,length(cx));                                         % hodnoty interpolacniho polynomu
    for k=1:length(cx)
        eL(k) = Lagr(cx(k),xx,val,i);                               % hodnota polynomu v bode cx(k) (st. polynomu je i-1)
    end

    eL2=zeros(1,length(cx));                                        % odhad chyby
    if i>1
        cx2 = xx(1) : (xx(i)-xx(1))/(length(cx)-1) : xx(i);
    else
        cx2=cx;
    end
    for k=1:length(cx)
        eL2(k)=Lagr(cx2(k),xx,val,i);
    end

    % nakreslime grafy
	subplot(2,1,1), plot(cx,funkce(cx),cx,eL);                      % puvodni funkce
	mf=max(abs(funkce(cx)));
	set(gca,'YLim',[-2*mf 2*mf]);
	hold on;
	plot(xx,val,'LineStyle','none','Marker','o','MarkerSize',6,'Color',[1 0 0]); % hodnoty ve znamych bodech
	hold off;
	subplot(2,1,2), plot(cx,funkce(cx)-eL);                         % interpolacni polynom
	s=get(gca,'YLim');
	set(gca,'YLim',[-max(abs(s)) max(abs(s))]);
	s=get(gca,'YLim');
	str=sprintf('Stupen polynomu: %d Norma rozdilu: %g',i-1,norm(funkce(cx2)-eL2));
	text(a+(b-a)/10,s(1)+s(2)/3,str,'FontSize',10);

    waitforbuttonpress;

end

end



%% Odpovez na otazky
% 1) Jakou z uvedenych testovacich funkci 'funkce(x)' lze podle tebe nejlepe
% aproximovat Lagrangeovym polynomem stupne 10
% Funkce 1 má s aproximací 10 stupně nejmenší normu rozdílu na daném
% intervalu, proto jí stanovíme jako nejlepší aproximací na intervalu.

% Uvazuj funkci y = exp(-x).*sqrt(abs(x))
% 2) Jaky stupen Lagr. polynomu na intervalu <-10,10> by sis vybral(a)
% Polynom stupně 13. Má nejmenší normu rozdílu z polynomů do stupně 20.
% 3) Kolik bodu (namerenych dat) je potreba pro konstrukci takoveho
% polynomu
% 14 naměřených bodů. (z grafu počet bodů na horním plotu anebo podle
% stupně polynomu n+1, kde +1 znázorňuje aproximaci konstantní fcí. aneb
% polynom stupně 0)
%
% pozn. lze vypozorovat z grafu, pokud vis, co zobrazuje
%




