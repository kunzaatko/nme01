#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from math import sqrt,sin,exp
import math
from mpl_toolkits import mplot3d
from triangle import Triangle
from random  import randint

def simplex(initial_triangle, function, trace=None, maximum_iteration_count = 1000, desired_precision = 0.01, get_max = False):
    if get_max:
        function_1 = lambda X : -1*function(X)
    else:
        function_1 = lambda X : function(X)
    methods = (Triangle.reflect,Triangle.expand,Triangle.contract)
    for method in methods:
        if method(initial_triangle,function_1):
            if not trace == None :
                trace.append(initial_triangle.copy())
            if initial_triangle.area() < desired_precision:
                initial_triangle.sort_WGB(function_1)
                return initial_triangle[2]
            elif maximum_iteration_count == 0:
                initial_triangle.sort_WGB(function_1)
                return initial_triangle[2]
            else:
                maximum_iteration_count = maximum_iteration_count-1
                simplex(initial_triangle,function_1,trace=trace,maximum_iteration_count=maximum_iteration_count,desired_precision=desired_precision,get_max=False)

    Triangle.reduce(initial_triangle,function_1)
    if not trace == None:
        trace.append(initial_triangle.copy())
    if initial_triangle.area() < desired_precision:
        initial_triangle.sort_WGB(function_1)
        return initial_triangle[2]
    elif maximum_iteration_count == 0:
        initial_triangle.sort_WGB(function_1)
        return initial_triangle[2]
    else:
        maximum_iteration_count = maximum_iteration_count-1
        simplex(initial_triangle,function_1,trace=trace,maximum_iteration_count=maximum_iteration_count,desired_precision=desired_precision,get_max=False)


def func_1(X):
    return X[0]**2-4*X[0]+X[1]**2-X[1]-X[0]*X[1]
def func_np_1(X,Y):
    return X**2-4*X+Y**2-Y-X*Y

def func_2(X):
    return 1 / sqrt(1 + X[0]*X[1]**2)
def func_np_2(X,Y):
    return 1 / np.sqrt(1 + X*Y**2)

def func_3(X):
    return sqrt(X[0]**2 + X[1]**2)
def func_np_3(X,Y):
    return np.sqrt(X**2 + Y**2)

def func_4(X):
    return (sin(X[0]+X[1]))/(X[0]**2+X[1]**2)
def func_np_4(X,Y):
    return np.sin(X+Y)/(X**2+Y**2)

def func_5(X):
    return X[0]**2*sin(2*X[1])/10
def func_np_5(X,Y):
    return X**2*np.sin(2*Y)/10

def func_6(X):
    return sin(X[0]) + sin(X[1])
def func_np_6(X,Y):
    return np.sin(X) + np.sin(Y)

def func_7(X):
    return X[1]**3-X[0]**3+X[0]
def func_np_7(X,Y):
    return X**3-Y**3+X

def func_8(X):
    return X[0]**2 - X[1]**2
def func_np_8(X,Y):
    return X**2 - Y**2

def func_9(X):
    return X[0]*X[1]*exp((-X[0]**2-X[1]**2))
def func_np_9(X,Y):
    return X*Y*np.exp(-X**2-Y**2)

functions = [[func_1,func_np_1],[func_2,func_np_2],[func_3,func_np_3],[func_4,func_np_4],[func_5,func_np_5],[func_6,func_np_6],[func_7,func_np_7],[func_8,func_np_8],[func_9,func_np_9]]

func = functions[8]
find_max = False

# ještě by se mělo ověřit, jestli není náhodou na přímce

# test_triangle =  Triangle([randint(-100,100),randint(-100,100)],[randint(-100,100),randint(-100,100)],[randint(-100,100),randint(-100,100)])
test_triangle =  Triangle([randint(-3,3),randint(-3,3)],[randint(-3,3),randint(-3,3)],[randint(-3,3),randint(-3,3)])
# test_triangle =  Triangle([randint(0,100),randint(0,100)],[randint(0,100),randint(0,100)],[randint(0,100),randint(0,100)])

visited = []
print(simplex(test_triangle,func[0],trace=visited,maximum_iteration_count=10,desired_precision=0.1,get_max=find_max))


fig = plt.figure()
gs = fig.add_gridspec(1,2)
ax_1 = fig.add_subplot(gs[0,0])
ax_2 = fig.add_subplot(gs[0,1],projection='3d')

# 3D axes
points = []
for triangle in visited:
    for point in triangle:
        points.append(point)
points_x,points_y= [point[0] for point in points],[point[1] for point in points]
points_z = [func[0]([x,y]) for x,y in zip(points_x,points_y)]
max_x,min_x,max_y,min_y = max(points_x),min(points_x),max(points_y),min(points_y)
max_border,min_border = max(max_x,max_y),min(min_x,min_y)

# max_x = max([point[1] for point in points])
axis_division = np.linspace(min_border,max_border,2000)
X = axis_division[:,None]
Y = axis_division[None,:]
Z = func[1](X,Y)
ax_2.plot_wireframe(X,Y,Z, alpha=0.3)


for triangle in visited:
    Triangle.plot(Triangle(triangle[0],triangle[1],triangle[2]),ax_1)
    Triangle.plot(Triangle(triangle[0],triangle[1],triangle[2]),ax_2, function=func[0], param_dict={'marker':''})

fig.show()

