#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

"""
Triangle class with methods for simplex implemetation.
"""

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits import mplot3d


def mid(A,B):
    """
    points are in np.array form
    """
    return (A+B)/2

def norm(X):
    """
    X is in the form of np.array. The norm is euklidean.
    """
    return math.sqrt(sum(X**2))

class Triangle(list):

    def __init__(self,pt_1,pt_2,pt_3):
        self.append(np.array(pt_1)); self.append(np.array(pt_2)); self.append(np.array(pt_3))

    def area(self):
        triangle = self.copy()
        triangle.append(triangle[0])
        segments = []
        for i in range(3):
            segments.append([triangle[i],triangle[i+1]])
        segment_lengths = []
        for segment in segments:
            segment_lengths.append(norm(segment[0]-segment[1]))
        s = sum(segment_lengths)/2
        return math.sqrt(s*math.prod([s-segment_length for segment_length in segment_lengths]))

    def set_self(self, triangle):
        self.clear()
        for point in triangle:
            self.append(point)


    def sort_WGB(self, func):
        """
        sorted in the order [Worst,Good,Best]
        """
        triangle = self.copy(); sorted_triangle = []
        for triangle_point in triangle[0:-1]:# {{{
            # print("This is the triangle", triangle, sep="->")
            index_max = np.argmax(np.array([func(point) for point in triangle]))
            # print("This we are sorting",[func(point) for point in triangle])
            # print("This index is the max", index_max)
            sorted_triangle.append(triangle[index_max])
            triangle.pop(index_max)
        sorted_triangle.append(triangle[0])
        # print("This is the sorted triangle", sorted_triangle, sep="->")
        self.set_self(sorted_triangle)# }}}


    def reflect(self, func):
        """# {{{
        returns False if Reflection_point is worse than the current worst point
        """
        self.sort_WGB(func)
        triag = self.copy()
        W,G,B = (point for point in triag)
        M = mid(G,B)
        R = 2*M - W
        if func(R) < func(W):
            self.set_self(Triangle(R,G,B))
            return self
        else:
            return False# }}}

    def expand(self, func):
        """# {{{
        returns False if Expantion_point is worse than the current worst point
        """
        self.sort_WGB(func)
        triag = self.copy()
        W,G,B = (point for point in triag)
        M = mid(G,B)
        R = 2*M - W
        E = 2*R - M
        if func(E) < func(W):
            self.set_self(Triangle(E,G,B))
            return self
        else:
            return False# }}}

    def contract(self, func):
        """# {{{
        returns False if Contraction_point is worse than the current worst point
        """
        self.sort_WGB(func)
        triag = self.copy()
        W,G,B = (point for point in triag)
        M = mid(G,B)
        R = 2*M - W
        C_1 = mid(R,M)
        C_2 = mid(W,M)
        if func(C_1) < func(C_2):
            C = C_1
        else:
            C = C_2
        if func(C) < func(W):
            self.set_self(Triangle(C,G,B))
            return self
        else:
            return False# }}}

    def reduce(self, func):
        """# {{{
        Returns the reduced triangle
        """
        self.sort_WGB(func)
        triag = self.copy()
        W,G,B = (point for point in triag)
        S = mid(W,B)
        M = mid(B,G)
        self.set_self(Triangle(S,M,B))
        return self# }}}

    def plot(self,axes,function = None,param_dict={'marker':'o'}):
        """# {{{
        3D:
        If the axes is 3D, then is returns the X,Y coordinates evaluated with function for the Z coordinate

        2D:
        If the axes is set and function is not 'None' then it returns a tuple (out_plot, triag_plot_Z), where triag_plot_Z is the coordinates X,Y evaluated with function
        """
        triag_plot = self.copy()
        triag_plot.append(triag_plot[0]) # dokončení poslední úsečky trojuhelníku
        triag_plot_X = [point[0] for point in triag_plot]
        triag_plot_Y = [point[1] for point in triag_plot]
        triag_plot_Z = None
        if not function == None:
            triag_plot_Z = [function([x,y]) for x,y in zip(triag_plot_X,triag_plot_Y)]
        if str(type(axes)) == "<class 'matplotlib.axes._subplots.Axes3DSubplot'>":
            out = axes.plot(triag_plot_X,triag_plot_Y,triag_plot_Z, **param_dict)
        else:
            out = axes.plot(triag_plot_X, triag_plot_Y,**param_dict)
        if triag_plot_Z:
            return (out, triag_plot_Z)
        else:
            return (out)# }}}

