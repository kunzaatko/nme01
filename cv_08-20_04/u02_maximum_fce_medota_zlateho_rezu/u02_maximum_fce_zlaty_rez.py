#! /bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math

def func_f(x):
    return x/2 - 0.02*x**2
func_f_num = np.vectorize(func_f)

# nefunknčí verze
# def golden_ratio(func,a,d,b=-10000000000,c=10000000000,get_max=False, iter_num=1000, record=[], inter_len = 0.00001):{{{
#     global gr # first iteration only -- setting the four points from the interval provided
#     if b == -10000000000:
#         b = a + (d-a)*gr
#     if c == 10000000000:
#         c = d - (d-a)*gr
#     if get_max == True:
#         alt_func = lambda x: -func(x) # mirror the axes if we are looking for a max rather than a min
#         golden_ratio(alt_func,a,d,b,c,get_max=False,iter_num=1000,record=[]) # calculate min for reversed func
#         for iteration in record: # if we were looking for a max rather than a min, we have to rotate the axis back to normal
#             for point in iteration:
#                 point[1] = -point[1]
#     if func(b) < func(c) and b-a > inter_len and iter_num > 0:
#         record.append([[a,func(a)],[b,func(b)],[c,func(c)],[d,func(d)]])
#         golden_ratio(func, a, c, c = b, b = -10000000000, get_max=False,iter_num=iter_num-1,record=record)
#     elif func(c) < func(b) and b-a > inter_len and iter_num > 0:
#         record.append([[a,func(a)],[b,func(b)],[c,func(c)],[d,func(d)]])
#         golden_ratio(func, b, d, b = c, c = 10000000000, get_max=False,iter_num=iter_num-1,record=record)
#     else:
#         return (a,d)}}}

def golden_ratio(func, a, d, iter_num=10000, get_max=False, trace=[], inter_len=0.0001):
    gr = (3-math.sqrt(5))/2
    b = a + (d-a)*gr
    c = d - (d-a)*gr

    # hledáme maximum a ne minimum -> otočíme si funkci a budeme na ní hledat minimum. Poté ji otočíme ve výsledku nazpět.
    if get_max == True:
        alt_func = lambda x: -func(x)
    else:
        alt_func = lambda x: func(x)

    for i in range(iter_num):
        if alt_func(b) > alt_func(c):
            if d-b < inter_len:
                right = True
                break
            trace.append([[a,alt_func(a)],[b,alt_func(b)],[c,alt_func(c)],[d,alt_func(d)]])
            (a,b,c,d) = (b,c,d - (d-b)*gr,d)
            right = True
        elif alt_func(c) > alt_func(b):
            if c-a < inter_len:
                right = False
                break
            trace.append([[a,alt_func(a)],[b,alt_func(b)],[c,alt_func(c)],[d,alt_func(d)]])
            (a,b,c,d) = (a,a + (c-a)*gr,b,c)
            right = False
    if get_max == True: # pokud hledáme maximum a ne minimum musíme tak jsme si na začátku převrátili funkci, ale stejně chceme, aby se vraceli hodnoty funkce, kterou jsme zadali a ne té pomocné
        for iteration in trace:
            for point in iteration:
                point[1] = -point[1]
    if right:
        return (b,d)
    else:
        return (a,c)

trace = []
inter = golden_ratio(func_f,0,20,get_max=True,trace=trace)
x = np.linspace(0,20,100000)
fig = plt.figure()
axes = fig.add_axes([0.05,0.05,0.9,0.9])
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('Optimalizace zlatým řezem')
axes.plot(x,func_f_num(x),'--', label='Funkce')
for ind,entry in enumerate(trace):
    print(ind,entry, sep=' -> ')
    x_trace = [point[0] for point in entry]
    y_trace = [point[1] for point in entry]
    axes.plot(x_trace, y_trace,'o-', label='{}. iterace'.format(ind+1))
axes.grid(True)
axes.legend(loc=2)
fig.show()
print("Maximální hodnota funkce je v intervalu", inter)
