#! /bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math

def funkce_f(x):
    return x/2 - 0.02*x**2
funkce_f_num = np.vectorize(funkce_f)

def diference(funkce, step=0.001):
    return lambda x: (funkce(x+step) - funkce(x))/step

def puleni_intervalu(funkce, a,b, presnost = 0.00005, visited = []):
    if ((funkce(a) > 0 and funkce(b) > 0) or (funkce(a) < 0 and funkce(b) < 0)):
        print("Špatně zadaný interval!")
    if abs(funkce(a)) < presnost:
        visited.append([a,funkce(a)])
        return a
    elif abs(funkce(b)) < presnost:
        visited.append([b,funkce(b)])
        return b
    else:
        c = (a+b)/2
        # jsme na intervalu (0,b) nebo (a,0) a musíme zjistit, jaká je reálná hodnota funkce v daném bodě. Nemůžeme použít podíl -> nedefinované chování
        if a == 0:
            if funkce(c) > 0:
                print(c, '->', a, sep = " ")
                visited.append([a,funkce(a)])
                puleni_intervalu(funkce, c, b, presnost, visited)
            else:
                print(c, '->', a, sep = " ")
                visited.append([b,funkce(b)])
                puleni_intervalu(funkce, a, c, presnost, visited)

        # můžeme vpořádku použít dělení. Máme na to dobrý interval.
        if funkce(c)/a > 0:
            print(c, '->', a, sep = " ")
            visited.append([a,funkce(a)])
            puleni_intervalu(funkce, c, b, presnost, visited)
        else:
            print(c, '->', a, sep = " ")
            visited.append([b,funkce(b)])
            puleni_intervalu(funkce, a, c, presnost, visited)

x = np.linspace(0,20,1000)

diference_funkce = diference(funkce_f_num)
# diference_funkce_num = np.vectorize(diference_funkce)

visited = []
puleni_intervalu(diference_funkce, 0, 20, presnost=0.0001, visited=visited)

figure = plt.figure()
axes_funkce = figure.add_axes([0.05, 0.05,0.9,0.9])
axes_funkce.set_xlabel('x')
axes_funkce.set_ylabel('y')
axes_funkce.set_title('Optimalizace diferencí')
axes_funkce.plot(x,funkce_f_num(x), '.-', label='funkce')
axes_funkce.plot(x,diference_funkce(x),'-',  label='derivace')
for ind,bod in enumerate(visited):
    axes_funkce.plot(bod[0],bod[1],'o')
    if bod != visited[-1]:
        axes_funkce.annotate('{}'.format(ind+1)+'.',xy=(bod[0],bod[1]), xytext=(bod[0]-0.1,bod[1]-0.1))
    else:
        axes_funkce.annotate('{}. = {}'.format(ind+1, bod[0]), xy=(bod[0],bod[1]), xytext=(bod[0]+0.1,bod[1]+0.1))
        axes_funkce.plot(bod[0],funkce_f(bod[0]),'x')

axes_funkce.grid(True)
axes_funkce.legend(loc=2)
figure.show()
