% Simplex - Otázka, Poznámka
% Martin Kunz
% 14 May 2020

# Komentář
* Bohužel stále nevím, jak bych udělal trojúhelníky v prostoru. Zatím je na mě numpy trochu neprobádaná oblast. Nemáš náhodou nějaký dobrý zdroj, kde bych se mohl numpy a matplotlib lépe naučit?

# Otázka
 * Mám trochu problém se svým programem. Nechce mi počítat správně, pokud ho spustím ze stejné instance pythonu několikrát. Asi to bude problém se zadáváním počátečního trojúhelníku pomocí náhody. Nevím jestli není možné, že by se celý nepřepsal, nebo tak podobně. Asi by se hodilo celý ten program přepsat. Je dost krkolomný, ale dělat to nebudu z časových důvodů. Nevíš, v čem by mohl být ten problém?

 Díky,\
 *Martin Kunz*

