#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib
import math
from random import randint

# Copyright © 2020 Martin Kunz <martinkunz@email.cz>{{{

# Distributed under terms of the GPL3 license.

# implementation of the simplex numerical method of optimization

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.}}}

def func_norm(x):
    return x[0]**2-4*x[0]+x[1]**2-x[1]-x[0]*x[1]
# def func_two_args(x,y):
#     return func_norm((x,y))
def func_np(x,y):
    return x**2-4*x+y**2-y-x*y

# metody trojuhelníka {{{
def mid(A,B):
    return [(A[0]+B[0])/2,(A[1]+B[1])/2]

def reflect(triag_in):
    """ # reflect_help {{{
    triag := [Worst,Good,Best]
    in := triag_in
    out := triag_out
    """ # }}}
    move = [mid(triag_in[1],triag_in[2])[0]-triag_in[0][0],mid(triag_in[1],triag_in[2])[1]-triag_in[0][1]]
    return [[mid(triag_in[1],triag_in[2])[0]+move[0],mid(triag_in[1],triag_in[2])[1]+move[1]],triag_in[1],triag_in[2]]

def expand(triag_in):
    """ # expand_help {{{
    triag := [Worst,Good,Best]
    in := triag_in
    out := triag_out
    """# }}}
    move = [(mid(triag_in[1],triag_in[2])[0]-triag_in[0][0])*2,(mid(triag_in[1],triag_in[2])[1]-triag_in[0][1])*2]
    return [[mid(triag_in[1],triag_in[2])[0]+move[0],mid(triag_in[1],triag_in[2])[1]+move[1]],triag_in[1],triag_in[2]]

def contract(triag_in):
    """ # contract_help {{{
    triag := [Worst,Good,Best]
    in := triag_in
    out := triag_out
    """# }}}
    return [mid(mid(triag_in[1],triag_in[2]),triag_in[0]),triag_in[1],triag_in[2]]

def reduce(triag_in):
    """ # reduce_help {{{
    triag := [Worst,Good,Best]
    in := triag_in
    out := triag_out
    """# }}}
    return [triag_in[0],mid(triag_in[0],triag_in[1]),mid(triag_in[2],triag_in[0])]

# trochu faux pas{{{
# def triag_area(triag_in):
#     area = 0
#     for point_A,point_B in zip(triag_in[:]+triag_in[0:1], triag_in[1:]+triag_in[0:2]):
#         area += (point_A[0]-point_B[0])**2+(point_A[1]-point_B[1])**2
#     return area}}}

def seg_len(A,B):
    return (A[0]-B[0])**2+(A[1]-B[1])**2

def triag_area(triag_in):
    return (seg_len(triag_in[0],triag_in[1])*seg_len(mid(triag_in[0],triag_in[1]),triag_in[2]))/2

def plot_triag(ax, triag, param_dict={'marker':'o'}):
    triag_plot = triag.copy()
    triag_plot.append(triag[0]) # dokončení poslední úsečky trojuhelníku
    triag_plot_X = [point[0] for point in triag_plot]
    triag_plot_Y = [point[1] for point in triag_plot]
    out = ax.plot(triag_plot_X, triag_plot_Y,**param_dict)
    return out

def plot_triag_3d(ax, triag, func=None, param_dict={}):
    triag_plot = triag.copy()
    # potřebujeme dopočítat hodnoty funkce v bodech trojúhelníku
    print("ploting triangle", triag_plot)
    if func:
        for i in range(3):
            triag_plot[i].append(func(triag_plot[i][0:2]))
    else:
        for i in range(3):
            triag_plot[i].append(0)
    print("ploting triangle", triag_plot)
    triag_plot.append(triag_plot[0]) # chceme spojit 4 body, ale nechceme si měnit zadaný trojúhelník o třech bodech
    triag_plot_X = [point[0] for point in triag_plot]
    triag_plot_Y = [point[1] for point in triag_plot]
    triag_plot_Z = [point[2] for point in triag_plot]
    print("X", triag_plot_X, sep="->")
    print("Y", triag_plot_Y, sep="->")
    print("Z", triag_plot_Z, sep="->")
    out = ax.plot(triag_plot_X[0:5], triag_plot_Y[0:5], triag_plot_Z[0:5])
    return out

# }}}

fig = plt.figure()
gs = fig.add_gridspec(1,2)
ax = fig.add_subplot(gs[0,0],title='V rovině')
ax_1 = fig.add_subplot(gs[0,1],title='V prostoru',projection='3d')

def simplex(func,triag_in,max_area = 0.1, get_max=False, trace=[]):# {{{
    """ # simplex_help {{{
    func := "function to simplex"
    triag := [point1,point2,point3]
    triag_in := "initial triangle"
    max_area := "maximum area for the output triangle"
    get_max := bool "find the maximum instead of the minimum"
    max_iter := int "maximum number of iterations"
    trace := list "list to save the triangles visited"
    out := "output triangle"
    """# }}}
    triag_in.sort(key=func,reverse=True)
    if max_area > triag_area(triag_in):    # pokud jsme dosáhli cílené přesnosti
        trace.append(triag_in)  # přidáme výsledný trojúhelník do seznamu navštívených
        print("výsledný trojúhelník",triag_in, sep='->')
        return triag_in     # vrátím hodnotu výsledného trojuhelníku
    else:
        R = reflect(triag_in)   # reflexe trojuhelníku je R
        if func_norm(R[0]) < func_norm(triag_in[0]):     # f(R->Worst) < f(in->Worst)
            E = expand(triag_in)    # expanze trojuhelníku je E
            if func_norm(E[0]) < func_norm(R[0]):
                trace.append(triag_in)
                triag_in = E
                print("expand",triag_in,sep=' -> ')
                simplex(func, triag_in,max_area,False,trace=trace)
            else:
                trace.append(triag_in)
                triag_in = R
                print("reflect",triag_in,sep=' -> ')
                simplex(func, triag_in,max_area,False,trace=trace)

        C = contract(triag_in)
        if func_norm(C[0]) < func_norm(triag_in[0]):
            trace.append(triag_in)
            triag_in = C
            print("contract",triag_in,sep=' -> ')
            simplex(func, triag_in,max_area,False,trace=trace)
        else:
            R = reduce(triag_in)
            trace.append(triag_in)
            triag_in = R
            print("reduce",triag_in,sep=' -> ')
            simplex(func, triag_in,max_area,False,trace=trace)# }}}

# Zkouška různých počátečních trojuhelníků{{{
# for i in range(40):
#     trace = []
#     triag_in=[[random.randint(-100,100),random.randint(-100,100)] for i in range(3)]
#     simplex(func_norm,triag_in,max_area=0.1, max_iter=10000, trace=trace)
#     plot_triag(ax,trace[-1],param_dict={'marker':''})
#     ax.annotate("{}".format(round(func_norm(trace[-1][2]),1)), xy = tuple(trace[-1][2]))}}}

# ax.legend()

# z nějakého důvodu to probíhá ten samý trojuheník několikrát. Je potřeba najít chybu v algoritmu, případně ho přepsat

# triag_in = [[90,39],[20,58],[30,-80]]
triag_in = [[randint(-100,100),randint(-100,100)] for i in range(3)]
trace = []
simplex(func_norm,triag_in,trace=trace)
for i,triag in enumerate(trace[0:-2]):
    plot_triag(ax,triag,{'marker':'','label':'{}'.format(i+1)})
    # ax.annotate('{}'.format(i+1),xy = [triag[0][0],triag[0][1]], xytext = [triag[0][0]+0.1,triag[0][1]+0.1])
plot_triag(ax,trace[-1],{'marker':'o','label':'výsledný trojuhelník'})
ax.annotate('obsah: {}, extrém: [{}, {}]'.format(round(triag_area(trace[-1]),2),round(trace[-1][2][0],4),round(trace[-1][2][1],4)),xy=trace[-1][2],xytext=[trace[-1][2][0]+0.1,trace[-1][2][1]+0.1])

axis_division = np.linspace(-100,100,200)
Z = func_np(axis_division[:,None],axis_division[None,:])
# Z = np.linspace([func_norm([X,Y]) for X,Y in zip(X,Y)])
ax_1.plot_wireframe(axis_division[:,None],axis_division[None,:],Z, alpha=0.3)

# for i,triag in enumerate(trace[-10:-2]):
#     plot_triag_3d(ax_1,triag, func=func_norm)
# plot_triag_3d(ax_1,trace[-1], func=func_norm)
# plot_triag_3d(ax_1,trace[-1])


fig.show()
