#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib
import re

def func(x):
    return math.sin(x)*math.pow(math.e,math.cos(x))

point_num = 1002
interval = 0,math.pi

def rectangle_approx(func,iterval,point_num):
    X = np.linspace(interval[0],interval[1],point_num).tolist()
    h = (interval[1]-interval[0])/(point_num-1)
    res = 0
    for x1,x2 in zip(X[0:-1],X[1:]):
        res = res + h*func((x1+x2)/2)
    return res

def trapezoid_approx(func,interval,point_num):
    X = np.linspace(interval[0],interval[1],point_num).tolist()
    h = (interval[1]-interval[0])/(point_num-1)
    res = 0
    for x1,x2 in zip(X[0:-1],X[1:]):
        res = res + h*(func(x1)+func(x2))/2
    return res

def simpsons_approx(func,interval,point_num):
    X = np.linspace(interval[0],interval[1],point_num).tolist()
    h = (interval[1]-interval[0])/(point_num-1)
    res = 0
    for x1,x2 in zip(X[0:-1],X[1:]):
        res = res + h*(func(x1)+4*func((x1+x2)/2)+func(x2))/6
    return res


print('obdelníková aproximace při {} bodech'.format(point_num-2),rectangle_approx(func,interval,point_num),sep='\n')
print('lichoběžníková aproximace aproximace při {} bodech'.format(point_num-2),trapezoid_approx(func,interval,point_num),sep='\n')
print('Simpsonova aproximace při {} bodech'.format(point_num-2),simpsons_approx(func,interval,point_num),sep='\n')

def plot(func,interval,max_point_num=10):
    fig = plt.figure()
    ax = fig.add_axes([0.05,0.05,0.9,0.9])
    number_of_points = list(range(3,max_point_num+3))

    methods = [rectangle_approx,trapezoid_approx,simpsons_approx]
    for method in methods:
        ax.plot([number_of_points-2 for number_of_points in number_of_points],[method(func,interval,number_of_points) for number_of_points in number_of_points],label='{}'.format(re.split(r'\s', repr(method).strip('<function'))[1]))

    ax.legend()
    fig.show()

plot(func,interval)

