#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math
import scipy.integrate as integ

def func(x):
    return math.sin(x)*math.e**math.cos(x)

func_np = np.vectorize(func)

def monte_carlo_rectangle_sum(func,interval,point_num = 10000):
    a = interval[0]
    b = interval[1]
    points = [random.random()*(b-a) + a for i in range(point_num)]
    sum_of_rects = 0
    for point in points:
        sum_of_rects = sum_of_rects + func(point)*(b-a)
    return sum_of_rects/point_num

interval = (0,math.pi)
number_of_points = 1000000
monte = monte_carlo_rectangle_sum(func,interval,point_num=number_of_points)
print('Metodou monte carlo je integrál při použití {} bodů: {}, což je {} od scipy integrálu podle scipy gaussových kvadratur 10. stupně'.format(number_of_points,monte ,integ.fixed_quad(func_np,0,math.pi,n=10)[0]-monte))
