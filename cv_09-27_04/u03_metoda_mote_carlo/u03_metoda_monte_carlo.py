#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math
import random


def func(x):
    return math.sin(x)*math.e**math.cos(x)

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])
def monte_carlo(func, a, b, point_num = 10000, plot = True,ax=None, param_dict={'marker':'x'}):
    global fig
    c = max([func(i) for i in list(np.linspace(a,b,1000))])+2
    rec_area = (b-a)*c
    in_int  = 0
    out_int = 0
    if plot:
        ax.plot(list(np.linspace(a,b,100)),[func(i) for i in list(np.linspace(a,b,100))],lw=3,color='green')
    for k in range(point_num):
        point = (random.random()*(b-a)+a, random.random()*c)
        if point[1] <= func(point[0]):
            in_int = in_int + 1
            if plot:
                ax.plot(point[0],point[1],**param_dict, color='blue')
        else:
            out_int = out_int + 1
            if plot:
                ax.plot(point[0],point[1],**param_dict, color='red')
    result = rec_area * (in_int/(out_int+in_int))
    # ax.annotate('integral from {} to {} using {} points using monte-carlo is -> {}'.format(a,b,point_num,round(result,4)), xy=[(b-a)/2,func((b-a)/2)], xytext=[(b-a)/2,func((b-a)/2)+1])
    # ax.title('integral from {} to {} using {} points using monte-carlo is -> {}'.format(a,b,point_num,round(result,4)))
    return result

print('metodou monte carlo -> ', monte_carlo(func,0,math.pi,ax=ax))
fig.show()

