#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from random import random
import scipy.integrate

def func(x_y):
    return x_y[0] + x_y[1]

def monte_carlo_3D(func,interval_x,interval_y,point_num = 1000000):
    a = interval_x[0]
    b = interval_y[0]
    x_inter_len = interval_x[1]-interval_x[0]
    y_inter_len = interval_y[1]-interval_y[0]
    sum_volume = 0
    for i in range(point_num):
        sum_volume = sum_volume + func([random()*x_inter_len+a,random()*y_inter_len+b])*x_inter_len*y_inter_len
    return sum_volume/point_num

interval_x = (0,1)
interval_y = (0,1)

integral_monte_carlo = monte_carlo_3D(func,interval_x,interval_y)

fig = plt.figure()
ax = fig.add_axes([0.05,0.05,0.9,0.9])

from_number = 1
to_number = 10000
number_of_points = 100
ax.set_title(r'Precision of the monte carlo method on the integral $\int\int_{[0,0]}^{[1,1]} \quad x + y \quad dy dx$')
ax.grid()
ax.set_xlabel('# of points')
ax.set_ylabel('error')
xs = list(np.linspace(from_number,to_number,number_of_points))[1:]
precision = [monte_carlo_3D(func,interval_x,interval_y,point_num=int(number)) - 1 for number in xs]

ax.plot(xs,precision,marker='x',color='red')
fig.show()

