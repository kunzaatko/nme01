#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import numpy as np
import math
import scipy.special
import scipy.integrate

def func(x):
    return np.sin(x)*np.power(np.e,np.cos(x))
func_np = np.vectorize(func)

interval = (0,math.pi)
def legendre_gauss_quadratture(func, number_of_points, interval):
    """
    compute the legedre_gauss_quadratture aproximation for function=func, number of poits=number_of_points on the interval interval
    """
    (points,weights) = scipy.special.roots_legendre(number_of_points)
    res = 0
    a = interval[0]
    b = interval[1]
    for c_i,x_i in zip(weights,points):
        res = res + c_i*(b-a)/2*func(((b-a)*x_i+(b+a))/2)

    return res

number_of_points = int(input("Zadej počet bodů: "))
print('legenreho-gaussova kvadratura na {}-ti bodech: '.format(number_of_points), legendre_gauss_quadratture(func,number_of_points,interval))
print('S řešením pomocí matlabu bohužel porovnávat nemohu. Scipy má ke každé funkci napsaný i typ aproximace, takže vím, že používá to samé akorát jiného řádu')
